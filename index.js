

//3
let number = prompt(`Enter a number`);
console.log(`The number you provided is ${number}.`)

//4

for (number; number > 0; number--){

	//5
	if(number === 50){
		console.log(`The current value is 50. Terminating the loop.`);
		break;
	}

	//6
	if(number % 10 === 0){
		console.log(`The number is divisible by 10. Skipping the number.`);
		continue;
	}

	//7
	if(number % 5 === 0){
		console.log(number);
	}


}

//8
let longWord = `superfragilisticalidocious`;
//9
let consonants = ``;

//10
for(let x = 0; x < longWord.length; x++){
	//11
	if (longWord[x] == `a` ||  longWord[x] == `e` || longWord[x] == `i` || longWord[x] == `o` || longWord[x] == `u`) {
		continue;
	}
	//12	
	else{
		consonants += longWord[x];
	}
}
	console.log(longWord);
	console.log(consonants);
